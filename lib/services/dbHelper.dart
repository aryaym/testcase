import 'package:majootestcase/models/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DbHelper {
  Future<Database> initDb() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, 'user_database.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT NOT NULL, email TEXT NOT NULL, password TEXT NOT NULL)",
        );
      },
      version: 1,
    );
  }

  Future<bool> insertUser(User user) async {
    // Get a reference to the database.
    final db = await initDb();

    var userExist = await checkUser(user);

    if (!userExist) {
      await db.insert(
        'users',
        user.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
      return true;
    } else {
      print('user is exist');
      return false;
    }
  }

  Future<bool> checkUser(User user) async {
    final db = await initDb();

    final userResult = await db.query('users',
        where: "email = ? AND password = ?",
        whereArgs: [user.email, user.password]);
    print('result is => $userResult');
    if (userResult.length > 0)
      return true;
    else
      return false;
  }
}
