import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';

class MovieDetail extends StatelessWidget {
  final Data data;

  const MovieDetail({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(25),
                child: Image.network(data.i.imageUrl),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text(data.l, textDirection: TextDirection.ltr),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text('${data.year}', textDirection: TextDirection.ltr),
              ),
              if(data.series != null)
              _series()
              else
                Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text("Opps, series not available"),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  Widget _series(){
    return SizedBox(
      height: 250,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: data.series.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: [
              Padding(
                padding: EdgeInsets.all(25),
                child: Image.network(data.series[index].i.imageUrl, height: 150, fit: BoxFit.fitHeight,),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text(data.series[index].l, textDirection: TextDirection.ltr),
              ),
            ],
          );
        },

      ),
    );
  }
}
