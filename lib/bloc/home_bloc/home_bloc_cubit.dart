import 'package:bloc/bloc.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetching_data() async {
    emit(HomeBlocLoadingState());
    ApiServices apiServices = ApiServices();
   MovieResponse movieResponse = await apiServices.getMovieList();
   await Future.delayed(Duration(seconds: 1));
    if(movieResponse==null){
       var hasInternet = await DataConnectionChecker().hasConnection;
       if(hasInternet)
        emit(HomeBlocErrorState("Error Unknown"));
       else
        emit(HomeBlocNoInternet());
    }else{
      emit(HomeBlocLoadedState(movieResponse.data));
    }
  }
}
